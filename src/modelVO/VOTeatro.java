
package modelVO;

import model.data.structures.ArbolBinario;
import model.data.structures.ListaEncadenada;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {

	/**
	 * Atributo que modela el nombre del teatro
	 */

	private String nombre;

	/**
	 * Atributo que modela la ubicacion del teatro 
	 */

	private VOUbicacion ubicacion;

	/**
	 * Atributo que referencia la franquicia
	 */

	private VOFranquicia franquicia;
	
	/**
	 * Atributo de �rbol de d�as
	 */
	private ArbolBinario<Integer, VOPeliculasDia> dias;


	public VOTeatro() {
		
		// TODO Auto-generated constructor stub
		dias = new ArbolBinario<Integer, VOPeliculasDia>();
		VOPeliculasDia peliculaDia;
		int cont = 0;
		for( int i = 0; i < 5; i++ )
		{
			peliculaDia = new VOPeliculasDia();
			dias.put(++cont, peliculaDia);
		}
	}


	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public void setUbicacion(VOUbicacion ubicacion){
		this.ubicacion = ubicacion;
	}

	public void setFranquicia(VOFranquicia franquicia){
		this.franquicia = franquicia;
	}


	public String getNombre(){
		return this.nombre;
	}

	public VOUbicacion getubicacion(){
		return this.ubicacion;
	}

	public VOFranquicia getFranquicia(){
		return this.franquicia;
	}
	
	public ArbolBinario<Integer, VOPeliculasDia> getDias(){
		return dias;
	}


}
