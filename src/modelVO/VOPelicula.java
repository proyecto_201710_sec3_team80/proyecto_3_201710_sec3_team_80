package modelVO;

public class VOPelicula {
	
	private Long id;
	private String titulo;
	private String[] generos;
	
	
	
	public VOPelicula(Long id, String titulo, String[] generos) {
	
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	
	

}
