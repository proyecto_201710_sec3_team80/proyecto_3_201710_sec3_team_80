package modelVO;

import model.data.structures.ArbolBinario;
import model.data.structures.ListaEncadenada;

public class VOPeliculasDia {
	
	private ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> listaMagnana; 
	private ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> listaTarde;
	private ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> listaNoche;
	
	public VOPeliculasDia( ){
		listaMagnana = new ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>>();
		listaTarde = new ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>>();
		listaNoche = new ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>>();
	}
	
	public ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> getMagnana( )
	{
		return listaMagnana;
	}

	public ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> getTarde( )
	{
		return listaTarde;
	}
	
	public ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> getNoche( )
	{
		return listaNoche;
	}
}
