package modelVO;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
	
	/**
	 * Atributo que modela el id del usuario
	 */

	private long id;
	
	public VOUsuario() {
		// TODO Auto-generated constructor stub
	}
	
	public long getID( )
	{
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
}
