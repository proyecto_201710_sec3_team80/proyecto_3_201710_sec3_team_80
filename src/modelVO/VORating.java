package modelVO;

import java.util.Comparator;

public class VORating implements Comparable<VORating> {

	private long idUsuario;
	private Long idPelicula;
	private double rating;
	private double recomendacion;
	
	public void setIdPelicula(Long idPelicula){
		this.idPelicula = idPelicula;
	}
	
	public Long getIdPelicula(){
		return this.idPelicula;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	public void setPrediccion( double pred )
	{
		this.recomendacion = pred;
	}
	public double getPrediccion( )
	{
		return recomendacion;
	}
	
	@Override
	public int compareTo(VORating arg0) {
		if( recomendacion == arg0.recomendacion )
		{
			return 0;
		}
		else if( recomendacion > arg0.recomendacion )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
}
