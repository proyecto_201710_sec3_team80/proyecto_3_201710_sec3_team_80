package model.data.structures;

import API.IDirectedEdge;
import API.IEdgeWeightedDigraph;
import recorridos.DijkstraSP;

public class EdgeWeightedDigraph implements IEdgeWeightedDigraph{

	private int V;
	private int E;
	private ListaEncadenada<DirectedEdge>[] adj;

	public EdgeWeightedDigraph(int v){
		this.V = v;
		this.E = 0;
		adj =  (ListaEncadenada<DirectedEdge>[]) new ListaEncadenada[v];
		for(int c =0; c < V; c++){
			adj[c] = new ListaEncadenada<DirectedEdge>();
		}
	}

	@Override
	public int V() {
		// TODO Auto-generated method stub
		return this.V;
	}

	@Override
	public int E() {
		// TODO Auto-generated method stub
		return this.E;
	}

	@Override
	public void addEdge(DirectedEdge e) {
		// TODO Auto-generated method stub
		
			
			adj[e.from()].agregarElementoFinal(e);
		E++;
	}

	@Override
	public Iterable<DirectedEdge> adj(int v) {
		// TODO Auto-generated method stub
		return adj[v];
	}

	@Override
	public Iterable<DirectedEdge> edges() {
		// TODO Auto-generated method stub
		ListaEncadenada<DirectedEdge> lista = new ListaEncadenada<DirectedEdge>();
		for(int v =0; v <V; v++)
		{
			for(DirectedEdge e: adj[v])
			{
				lista.agregarElementoFinal(e);
			}
		}
		return lista;
	}
	
	
	public ListaEncadenada<DirectedEdge> darAdjuntos(int v){
		return adj[v];
	}
	
	public DijkstraSP dijkstra(int v){
		DijkstraSP dijkstra = new DijkstraSP(this,v);
		return dijkstra;
	}
	
	public EdgeWeightedDigraph reverse() {
        EdgeWeightedDigraph reverse = new EdgeWeightedDigraph(V);
        for (int v = 0; v < V; v++) {
            for (DirectedEdge w : adj(v)) {
                reverse.addEdge(w);
            }
        }
        return reverse;
    }
	
}
