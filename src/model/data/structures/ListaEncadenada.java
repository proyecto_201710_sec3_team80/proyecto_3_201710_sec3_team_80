package model.data.structures;

import java.util.Comparator;
import java.util.Iterator;

import API.ILista;

public class ListaEncadenada<T>  implements ILista<T>{

	private NodoSencillo<T> primero;
	private NodoSencillo<T> actual;
	private NodoSencillo<T> ultimo;

	private int longitud;
	private int longitudAntes;

	public ListaEncadenada<T> aux;

	public ListaEncadenada(){
		primero = null;
		ultimo = primero;
		actual = primero;
		longitud =0;
	}

	public void cambiarActualPrimero()
	{
		actual = primero;
	}

	public boolean isEmpty(){
		if(longitud == 0)
			return true;
		else return false;
	}


	public T pop(){

		NodoSencillo<T> aux = new NodoSencillo<T>();
		aux = primero;
		primero = primero.darSiguiente();
		longitud--;
		return aux.darItem();

	}

	public void push(T item){

		NodoSencillo<T> aux = new NodoSencillo<T>();

		if(primero != null)
			aux.establecerSiguiente(primero);

		primero = aux;
		aux.establecerItem(item);
		longitud++;
	}

	public void enqueue(T item){
		NodoSencillo<T> oldLast = ultimo;
		ultimo = new NodoSencillo<T>();
		ultimo.establecerItem(item);
		ultimo.establecerSiguiente(null);
		if(isEmpty()) primero  = ultimo;
		else oldLast.establecerSiguiente(ultimo);
		longitud++;
	}

	public T dequeue(){
		T item  = primero.darItem();
		primero = primero.darSiguiente();
		if(isEmpty()) ultimo = null;
		longitud--;
		return item;
	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>(){
			private NodoSencillo<T> temp = primero;

			public boolean hasNext(){
				return temp != null;
			}

			@Override
			public T next(){
				T resultado = null;
				if(temp != null){
					resultado = temp.darItem();
					temp = temp.darSiguiente();
				}
				return resultado;
			}
		};
	}

	public void agregarElementoFinal(T elem) {

		NodoSencillo<T> nuevo = new NodoSencillo<T>();
		nuevo.establecerItem(elem);
		longitudAntes =longitud;
		if(primero == null){
			primero = nuevo;
			actual = primero;
			ultimo = primero;
			longitud++;
		}
		else{
			ultimo.establecerSiguiente(nuevo);
			ultimo = nuevo;
			longitud++;
		}
	}

	public boolean agrego( )
	{
		if( longitudAntes < longitud )
		{
			return true;
		}
		return false;
	}

	public NodoSencillo<T> darActual( )
	{
		return actual;
	}

	public T darElemento(int pos) {
		// TODO Auto-generated method stub

		if(actual != null){
			actual = primero;

			//El contador debe iniciar en 1 para que la pel�cula que se est� buscando coincida con la que se est� retonando
			//Si quedan dudas del funcionamiento pueden inicializarlo en 0, correr las pruebas y hacer debug para que vean el funcionamiento 
			int i =1;
			while(i < pos ){
				actual = actual.darSiguiente();
				i++;
			}

			return actual.darItem();
		}
		return null;

	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return longitud;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.darItem();
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actual == ultimo){
			return false;
		}
		actual = actual.darSiguiente();
		return true;

	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actual == primero){
			return false;
		}

		NodoSencillo<T> temp = new NodoSencillo<T>();
		temp = primero;
		boolean cambiado = false;
		for(int i =0; i < longitud && !cambiado ; i++){

			if(temp.darSiguiente() == actual){
				actual = temp;
				cambiado = true;
			}
			temp = temp.darSiguiente();
		}
		return cambiado;
	}

	public T eliminarElemento(int k){
		NodoSencillo<T> previous = null;
		T buscado = null;
		if(k == 0){	    
			buscado = primero.darItem();
			primero = primero.darSiguiente();
			longitud--;

		}else{

			int i = 0;
			NodoSencillo<T> current = primero;

			while (i < k && current != null){

				previous = current;
				current =current.darSiguiente();

				i++;
			}
			buscado = current.darItem();
			previous.establecerSiguiente(current.darSiguiente());
			longitud--;
		}
		return buscado;
	}

	public void concatenar(ILista<T> b)
	{
		if(!this.equals(b))
		{
			for(int i = 0;i < b.darNumeroElementos(); i++)
			{

				this.agregarElementoFinal(b.darElemento(i));
			}
		}
	}

	public T buscarLlave( T vo1, Comparator comp)
	{
		boolean encontro = false;
		NodoSencillo<T> actual = primero;
		T buscado = null;
		for( int i = 0; i < darNumeroElementos() && !encontro; i++ )
		{

			//System.out.println(v.getIdUsuario());
			//			System.out.println(comp.compare(actual.darItem(), vo1));
			if(  comp.compare(actual.darItem(), vo1) == 0 )
			{
				buscado = actual.darItem();
				encontro = true;
			}	
			else
			{
				actual = actual.darSiguiente();	
			}	
		}
		return buscado;
	}

	public void agregarElementoPosicion( int pos, T elem )
	{
		NodoSencillo<T> casillaActual = primero;
		NodoSencillo<T> casillaAnterior = null;
		for (int i = 0; i < pos; i++)
		{
			if( i == (pos-1) )
			{
				NodoSencillo<T> agregar = new NodoSencillo<>( );
				agregar.establecerItem(elem);
				agregar.establecerSiguiente(casillaActual);
			}
			casillaAnterior = casillaActual;

		}

	}

	/**
	 * Método que organiza una lista con el algoritmo MergeSort
	 * @param a Lista que se quiere ordenar
	 * @param comp Comparador del tipo que se requiera para comparar los elementos de la lista 
	 * @param order True para orden ascendente, False para orden descendente
	 */
	public void sort(ListaEncadenada<T> a, Comparator<T> comp, boolean order )
	{
		aux = new ListaEncadenada<T>(); 
		sort(comp, 0, a.darNumeroElementos() - 1, a, order);
	}


	private void sort(Comparator<T> comp, int lo, int hi, ListaEncadenada<T> a, boolean order) 
	{
		if (hi <= lo) 
			return;
		int mid = lo + (hi - lo)/2;
		sort(comp, lo, mid, a, order); // Sort left half.
		sort(comp, mid+1, hi, a, order); // Sort right half.
		merge(a, lo, mid, hi, comp, order);
	}

	private void merge(ListaEncadenada<T> a, int lo, int mid, int hi, Comparator<T> comp, boolean order)
	{
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
			aux.agregarElementoPosicion(k, a.darElemento(k));
		for (int k = lo; k <= hi; k++)
		{
			if( order )
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp.compare(aux.darElemento(j), aux.darElemento(i)) < 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
			else
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp.compare(aux.darElemento(j), aux.darElemento(i)) > 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
		}
	}

	/**
	 * Método que organiza una lista con el algoritmo MergeSort, se usa cuando se quiere 
	 * ordenarr dados 3 criterios cuando hay dos elemntos que tienen igual el primero o segundo criterio 
	 * NOTA: Los criterios deben ir en orden de prioridad según lo deseado
	 * @param a Lista que se quiere ordenar
	 * @param compPrimerCriterio Comparador del tipo del primer criterio que se requiera para comparar los elementos de la lista
	 * @param compSegundoCriterio Comparador del tipo del segundo criterio
	 * @param compTercerCriterio Comparador del tipo del tercer criterio  
	 * @param order True para orden ascendente, False para orden descendente
	 */
	public void sort3Criterios( ListaEncadenada<T> a, Comparator<T> compPrimerCriterio, Comparator<T> compSegundoCriterio, Comparator<T> compTercerCriterio, boolean order )
	{
		aux = new ListaEncadenada<T>(); 
		sort3Criterios(compPrimerCriterio, compSegundoCriterio, compTercerCriterio, 0, a.darNumeroElementos() - 1, a, order);
	}

	private void sort3Criterios( Comparator<T> comp1, Comparator<T> comp2, Comparator<T> comp3, int lo, int hi, ListaEncadenada<T> a, boolean order )
	{
		if (hi <= lo) 
			return;
		int mid = lo + (hi - lo)/2;
		sort3Criterios(comp1, comp2, comp3, lo, mid, a, order); // Sort left half.
		sort3Criterios(comp1, comp2, comp3, mid+1, hi, a, order); // Sort right half.
		merge3Criterios(a, lo, mid, hi, comp1, comp2, comp3, order);
	}

	private void merge3Criterios(ListaEncadenada<T> a, int lo, int mid, int hi, Comparator<T> comp1, Comparator<T> comp2, Comparator<T> comp3, boolean order)
	{
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) 
			aux.agregarElementoPosicion(k, a.darElemento(k));
		for (int k = lo; k <= hi; k++)
		{
			if( order )
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
				{
					if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
					{
						if( comp3.compare(aux.darElemento(j), aux.darElemento(i)) < 0 )
						{
							a.agregarElementoPosicion(k, aux.darElemento(j++));
						}
						else
						{
							a.agregarElementoPosicion(k, aux.darElemento(i++));
						}
					}
					else if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) < 0 )
					{
						a.agregarElementoPosicion(k, aux.darElemento(j++));
					}
					else
					{
						a.agregarElementoPosicion(k, aux.darElemento(i++));
					}
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) < 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
			else
			{
				if (i > mid) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else if (j > hi ) 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
				{
					if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) == 0 )
					{
						if( comp3.compare(aux.darElemento(j), aux.darElemento(i)) > 0 )
						{
							a.agregarElementoPosicion(k, aux.darElemento(j++));
						}
						else
						{
							a.agregarElementoPosicion(k, aux.darElemento(i++));
						}
					}
					else if( comp2.compare(aux.darElemento(j), aux.darElemento(i)) > 0 )
					{
						a.agregarElementoPosicion(k, aux.darElemento(j++));
					}
					else
					{
						a.agregarElementoPosicion(k, aux.darElemento(i++));
					}
				}
				else if( comp1.compare(aux.darElemento(j), aux.darElemento(i)) > 0 ) 
				{ 
					a.agregarElementoPosicion(k, aux.darElemento(j++));
				}
				else 
				{
					a.agregarElementoPosicion(k, aux.darElemento(i++));
				}
			}
		}
	}


}
