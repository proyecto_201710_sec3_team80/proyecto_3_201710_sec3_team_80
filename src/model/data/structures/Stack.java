package model.data.structures;

import java.util.Iterator;

import API.IStack;

public class Stack<T> implements IStack<T>, Iterable<T>{

	private ListaEncadenada<T> list;
	
	public Stack(){
		
		list = new ListaEncadenada<T>();
	}
	
	public boolean isEmpty(){return list.isEmpty();}
	
	public void push(T item){
		list.push(item);
	}
	
	public T pop(){
		return list.pop();
	}
	
	public int size(){return list.darNumeroElementos();}
	
	public Iterator<T> iterator(){
		return list.iterator();
	}
	
}
