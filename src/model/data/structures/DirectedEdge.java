package model.data.structures;

import API.IDirectedEdge;

public class DirectedEdge implements IDirectedEdge, Comparable<DirectedEdge>{

	private final int v;
	private final int w;
	private final double weight;

	public DirectedEdge(int v, int w, double weight){
		this.v = v;
		this.w = w;
		this.weight = weight;
	}
	@Override
	public double weight() {
		// TODO Auto-generated method stub
		return weight;
	}

	@Override
	public int from() {
		// TODO Auto-generated method stub
		return v;
	}
	@Override
	public int to() {
		// TODO Auto-generated method stub
		return w;
	}
	@Override
	public int compareTo(DirectedEdge o) {
		
		int retornar = -1;
		if(this.weight < o.weight()){retornar = -1;}
		else if(this.weight > o.weight()){retornar =1;}
		else{retornar =0;}
		return retornar;
	}

}
