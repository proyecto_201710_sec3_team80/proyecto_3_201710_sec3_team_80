package recorridos;
import java.util.*;

import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.ListaEncadenada;

public class MaxMatchingEdmonds {

	static int lca(int[] match, int[] base, int[] p, int a, int b) {
		boolean[] used = new boolean[match.length];
		while (true) {
			a = base[a];
			used[a] = true;
			if (match[a] == -1) break;
			a = p[match[a]];
		}
		while (true) {
			b = base[b];
			if (used[b]) return b;
			b = p[match[b]];
		}
	}

	static void markPath(int[] match, int[] base, boolean[] blossom, int[] p, int v, int b, int children) {
		for (; base[v] != b; v = p[match[v]]) {
			blossom[base[v]] = blossom[base[match[v]]] = true;
			p[v] = children;
			children = match[v];
		}
	}

	static int findPath( EdgeWeightedDigraph graph, int[] match, int[] p, int root) {
		int n = graph.V();
		boolean[] used = new boolean[n];
		Arrays.fill(p, -1);
		int[] base = new int[n];
		for (int i = 0; i < n; ++i)
			base[i] = i;
		int toInt = 0;

		used[root] = true;
		int qh = 0;
		int qt = 0;
		int[] q = new int[n];
		q[qt++] = root;
		while (qh < qt) {
			int v = q[qh++];

			for (DirectedEdge to : graph.adj(v)) {
				toInt = to.to();
				if (base[v] == base[toInt] || match[v] == toInt) continue;
				if (to.to() == root || match[toInt] != -1 && p[match[toInt]] != -1) {
					int curbase = lca(match, base, p, v, toInt);
					boolean[] blossom = new boolean[n];
					markPath(match, base, blossom, p, v, curbase, toInt);
					markPath(match, base, blossom, p, toInt, curbase, v);
					for (int i = 0; i < n; ++i)
						if (blossom[base[i]]) {
							base[i] = curbase;
							if (!used[i]) {
								used[i] = true;
								q[qt++] = i;
							}
						}
				} else if (p[toInt] == -1) {
					p[toInt] = v;
					if (match[toInt] == -1)
						return toInt;
					toInt = match[to.to()];
					used[toInt] = true;
					q[qt++] = toInt;
				}
			}
		}
		return -1;
	}

	public static int[] maxMatching(EdgeWeightedDigraph graph, ListaEncadenada<DirectedEdge> arcos) {
		int n = graph.V();
		int[] match = new int[n];
		Arrays.fill(match, -1);
		int[] p = new int[n];
		DirectedEdge e;
		for (int i = 0; i < n; ++i) {
			if (match[i] == -1) {
				int v = findPath(graph, match, p, i);
				while (v != -1) {
					
					int pv = p[v];
					int ppv = match[pv];
					e = new DirectedEdge(pv, v, 0.0);
					//match[v] = pv;
					match[pv] = v;
					v = ppv;
					arcos.agregarElementoFinal(e);
				}
			}
		}

		int matches = 0;
		for (int i = 0; i < n; ++i)
			if (match[i] != -1)
				++matches;
		return match;
	}
}