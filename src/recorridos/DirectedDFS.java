package recorridos;

import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.ListaEncadenada;

public class DirectedDFS {

	private boolean[] marked;
	
	public DirectedDFS(EdgeWeightedDigraph G, int s,ListaEncadenada<Integer> recorrido)
	{
		marked = new boolean[G.V()];
		dfs(G,s, recorrido);
	}
	
	public DirectedDFS(EdgeWeightedDigraph G, Iterable<Integer> sources, ListaEncadenada<Integer> recorrido)
	{
		marked = new boolean[G.V()];
		for(int s: sources)
			if(!marked[s]) dfs(G, s, recorrido);
	}
	
	private void dfs(EdgeWeightedDigraph G, int v, ListaEncadenada<Integer> recorrido)
	{
		marked[v] = true;
		recorrido.agregarElementoFinal(v);
		
		if(G.adj(v) != null){
		for(DirectedEdge w: G.adj(v))
			if(!marked[w.to()]) dfs(G, w.to(), recorrido);
		}
	}
	
	public boolean marked(int v)
	{
		return marked[v];
	}
	
}
