package logica;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.ArbolBinario;
import model.data.structures.EncadenamientoSeparadoTH;
import model.data.structures.ListaEncadenada;
import modelVO.VOFranquicia;
import modelVO.VOTeatro;
import modelVO.VOUbicacion;

public class CargarTeatro {

	public static boolean cargarTeatros(String ruta, ListaEncadenada<VOTeatro> listaNombres, EncadenamientoSeparadoTH<String, Integer> posicionesGrafo){
		boolean cargado = false;

		JsonParser parser = new JsonParser(); 
		try{

			JsonElement jelement = parser.parse(new FileReader(ruta));
			JsonArray jArray = jelement.getAsJsonArray();


			String nombre;
			String stringFranqu;
			VOUbicacion ubicacion;
			VOFranquicia franquicia;
			String stringUbicacion;
			String lat;
			String longi;

			JsonPrimitive name;
			JsonPrimitive Jfranqui; 
			JsonPrimitive ubica;

			JsonObject jobject;
			VOTeatro sala;

			for(int i =0; i < jArray.size(); i++){

				jobject = jArray.get(i).getAsJsonObject();

				name = jobject.getAsJsonPrimitive("Nombre");
				nombre = name.getAsString();

				ubica = jobject.getAsJsonPrimitive("Ubicacion geografica (Lat|Long)");
				stringUbicacion = ubica.getAsString();

				Jfranqui = jobject.getAsJsonPrimitive("Franquicia");
				stringFranqu = Jfranqui.getAsString();

				franquicia = new VOFranquicia();
				franquicia.setNombre(stringFranqu);

				ubicacion = new VOUbicacion();
				
				lat = stringUbicacion.split("[|]")[0];
				longi = stringUbicacion.split("[|]")[1];
				
				ubicacion.setLatitud(Double.parseDouble(lat));
				ubicacion.setLatitud(Double.parseDouble(longi));

				sala = new VOTeatro();

				sala.setNombre(nombre);
				sala.setUbicacion(ubicacion);
				sala.setFranquicia(franquicia);
				
				listaNombres.agregarElementoFinal(sala);
				posicionesGrafo.insertar(nombre, listaNombres.darNumeroElementos()-1);
				
			}
			cargado = true;

			System.out.println("Se cargaron las salas a partir del archivo, teatro_v3.json");
		}
		catch(Exception e){
			e.printStackTrace();
		}


		return cargado;
	}
}
