package logica;

import model.data.structures.ArbolBinario;
import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.ListaEncadenada;
import model.data.structures.MinPQDirectedEdge;
import modelVO.VOPeliculaPlan;
import modelVO.VOPeliculasDia;
import modelVO.VORating;
import modelVO.VOTeatro;
import recorridos.DijkstraSP;

public class Requerimiento8 {
	private double horaActual = 8;
	private  VOTeatro teatroAux;
	private  VOPeliculasDia carteleraDia;
	private  ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> peliculasFranja;
	private ArbolBinario<Long, VOPeliculaPlan> plan;
	private FestivalCine festival;
	private ListaEncadenada<VORating> mayoresRecomendados;
	private long idPelicula;
	private ListaEncadenada<VOPeliculaPlan> planPeliculas;

	public ListaEncadenada<VOPeliculaPlan> run(FestivalCine festival, ListaEncadenada<VORating> listaPeliculasGenero, EdgeWeightedDigraph G){

		this.festival = festival;

		mayoresRecomendados = listaPeliculasGenero;
		
		//for(int i =0; i < festival.sizeTeatros(); i++ ){
		ListaEncadenada<VOPeliculaPlan> listaFunciones;

		DijkstraSP dijkstraSP = new DijkstraSP(G, 0);
		DirectedEdge[] edgeTo = dijkstraSP.edgeTo();


		MinPQDirectedEdge<DirectedEdge> minHeapPeso;
		ArbolBinario<Long, VOPeliculaPlan>[] arregloPlanDiario= (ArbolBinario<Long, VOPeliculaPlan>[])new ArbolBinario[5];
		int n;
		MinPQDirectedEdge<DirectedEdge> minHeapAux;
		VOTeatro teatroAnterio = null;
		int horaSiguiente = 8;
		for(int h =1; h < 5;h++){

			minHeapPeso = new MinPQDirectedEdge<>(edgeTo.length);
			DirectedEdge edgeToV = new DirectedEdge(0, 0, 0.0);
			minHeapPeso.insert(edgeToV);

			for(int i =0; i < edgeTo.length; i++){
				if(edgeTo[i] != null)
					minHeapPeso.insert(edgeTo[i]);
			}


			DirectedEdge e;
			plan = new ArbolBinario<>();
			n = minHeapPeso.size();
			minHeapAux = new MinPQDirectedEdge<>(10);
			for(int i =0; horaActual < 24; i++){

				if(minHeapPeso.isEmpty())
					e = minHeapAux.delMin();
				else{
					e = minHeapPeso.delMin();
					minHeapAux.insert(e);
				}
				
				teatroAux = festival.darVOTeatro(e.to());
				
				if(teatroAnterio == null){
					teatroAnterio = teatroAux;
				
				}
				
				carteleraDia = teatroAux.getDias().get(h);
				if(horaSiguiente <12){
					peliculasFranja = carteleraDia.getMagnana();

					for(VORating recomendado: mayoresRecomendados){
						if(horaSiguiente >= 12){break;}
						idPelicula = recomendado.getIdPelicula();

						if(peliculasFranja.contains(idPelicula)){
							listaFunciones = peliculasFranja.get(idPelicula);
							for(VOPeliculaPlan funcion: listaFunciones){
								if(funcion.getHoraInicio() == horaSiguiente)
								{
									
									
									if(!plan.contains(idPelicula)){
										plan.put(idPelicula, funcion);
										horaActual += 2;
										horaActual += e.weight()/60;
										horaActual = Math.ceil(horaActual);
										if(funcion.getHoraFin() == 12){horaSiguiente+=2;}else{horaSiguiente = funcion.getHoraFin();}
										if(horaActual %2 != 0)horaActual++;
									}
								}
							}

						}
					}
				}
				if(12 <= horaSiguiente && horaSiguiente <18){
					peliculasFranja = carteleraDia.getTarde();

					for(VORating recomendado: mayoresRecomendados){
						if(horaSiguiente >= 18){break;}
						idPelicula = recomendado.getIdPelicula();

						if(peliculasFranja.contains(idPelicula)){
							listaFunciones = peliculasFranja.get(idPelicula);
							for(VOPeliculaPlan funcion: listaFunciones){
								if(funcion.getHoraInicio() == horaSiguiente)
								{
									
									if(!plan.contains(idPelicula)){
										plan.put(idPelicula, funcion);
										horaActual += 2;
										horaActual += (e.weight()+(e.weight()*0.15))/60;
										horaActual = Math.ceil(horaActual);
										if(funcion.getHoraFin() == 18){horaSiguiente+=2;}else{horaSiguiente = funcion.getHoraFin();}
										if(horaActual %2 != 0)horaActual++;
									}
								}
							}

						}
					}
				}
				if(18<= horaSiguiente && horaSiguiente < 24){
					peliculasFranja = carteleraDia.getNoche();

					for(VORating recomendado: mayoresRecomendados){
						if(horaSiguiente >= 24){break;}
						idPelicula = recomendado.getIdPelicula();

						if(peliculasFranja.contains(idPelicula)){
							listaFunciones = peliculasFranja.get(idPelicula);
							for(VOPeliculaPlan funcion: listaFunciones){
								if(funcion.getHoraInicio() == horaSiguiente)
								{
									
									if(!plan.contains(idPelicula)){
										horaSiguiente+=2;
										plan.put(idPelicula, funcion);
										horaActual += 2;
										horaActual += (e.weight()+(e.weight()*0.20))/60;
										horaActual = Math.ceil(horaActual);
										if(horaActual %2 != 0)horaActual++;
									}
								}
							}

						}
					}
				}
				else{

					break;

				}
				horaSiguiente+=2;
			}
			arregloPlanDiario[h] = plan;
		}

		//}
		planPeliculas = new ListaEncadenada<>();
		arregloPlanDiario[1].inOrderLista(planPeliculas);
		
		for(int i =1; i < arregloPlanDiario.length; i++){
			
			if(planPeliculas.darNumeroElementos() < arregloPlanDiario[i].size())
				arregloPlanDiario[i].inOrderLista(planPeliculas);
		}

		return planPeliculas;
	}
}
