package logica;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.ArbolBinario;
import model.data.structures.EncadenamientoSeparadoTH;
import model.data.structures.ListaEncadenada;
import modelVO.VOPelicula;
import modelVO.VOPeliculaPlan;
import modelVO.VOPeliculasDia;
import modelVO.VOTeatro;

public class CargarCarteleras {

	public static boolean cargarInfoCarteleras(ListaEncadenada<String> rutas, VOTeatro[] infoTeatros, EncadenamientoSeparadoTH<String, Integer> tabla, ArbolBinario<Long, VOPelicula> peliculas){

		boolean cargados = false;

		JsonParser parser = new JsonParser();

		JsonElement jelement;
		JsonArray jArray;

		JsonObject elemento;

		String nombreTeatro;
		JsonPrimitive jTeatros;

		JsonObject jTeatro;

		JsonArray arrayPeliculas;

		JsonObject jPelicula;

		JsonPrimitive jId;
		Long id;

		JsonArray arrayFunciones;

		JsonObject jFuncion;
		JsonPrimitive jHora;

		String hora;
		Integer time = 0;

		int posicion;
		VOTeatro actual;
		int dia = 0;
		VOPeliculaPlan peliculaHora;
		ListaEncadenada<VOPeliculaPlan> horariosPelicula;
		for(String ruta: rutas){
			//Dia actual
			dia++;
			try{

				jelement = parser.parse(new FileReader(ruta));
				jArray  = jelement.getAsJsonArray();


				for(int i =0; i < jArray.size(); i++){

					elemento = jArray.get(i).getAsJsonObject();
					jTeatros = elemento.getAsJsonPrimitive("teatros");
					nombreTeatro = jTeatros.getAsString();

					jTeatro = elemento.getAsJsonObject("teatro");

					arrayPeliculas = jTeatro.getAsJsonArray("peliculas");

					//Teatro actual
					posicion = tabla.darValor(nombreTeatro);
					System.out.println(nombreTeatro + " posicion: " + posicion + " dia: " + dia);
					actual = infoTeatros[posicion];

					for(int j=0; j < arrayPeliculas.size(); j++){
						jPelicula = arrayPeliculas.get(j).getAsJsonObject();

						//ID pelicula actual
						jId = jPelicula.getAsJsonPrimitive("id");
						id = jId.getAsLong();

						arrayFunciones = jPelicula.getAsJsonArray("funciones");


						for(int k =0; k < arrayFunciones.size(); k++){

							jFuncion = arrayFunciones.get(k).getAsJsonObject();
							jHora = jFuncion.getAsJsonPrimitive("hora");

							hora = jHora.getAsString();

							time = Integer.parseInt(hora.split("[:]")[0]);

							if(hora.indexOf('p') >0){
								if(time != 12){time+=12;}
							}


							peliculaHora = new VOPeliculaPlan();
							peliculaHora.setPelicula(peliculas.get(id));
							peliculaHora.setDia(dia);
							peliculaHora.setTeatro(actual);
							peliculaHora.setHoraInicio(time);

							if(time == 22 )
							{
								peliculaHora.setHoraFin(00);
							}
							else
							{
								peliculaHora.setHoraFin(time+2);
							}

							if( time < 12)
							{
								horariosPelicula = actual.getDias().get(dia).getMagnana().get(id);

								if(horariosPelicula == null){horariosPelicula = new ListaEncadenada<>(); horariosPelicula.agregarElementoFinal(peliculaHora); actual.getDias().get(dia).getMagnana().put(id, horariosPelicula);}

								else{horariosPelicula.agregarElementoFinal(peliculaHora);}

							}
							else if( 12 <= time && time < 18 )
							{
								horariosPelicula = actual.getDias().get(dia).getTarde().get(id);

								if(horariosPelicula == null){horariosPelicula = new ListaEncadenada<>(); horariosPelicula.agregarElementoFinal(peliculaHora); actual.getDias().get(dia).getTarde().put(id, horariosPelicula);}

								else{horariosPelicula.agregarElementoFinal(peliculaHora);}
								
							}
							else if( time > 18 )
							{
								horariosPelicula = actual.getDias().get(dia).getNoche().get(id);

								if(horariosPelicula == null){horariosPelicula = new ListaEncadenada<>(); horariosPelicula.agregarElementoFinal(peliculaHora); actual.getDias().get(dia).getNoche().put(id, horariosPelicula);}

								else{horariosPelicula.agregarElementoFinal(peliculaHora);}
							}
							
						}

					}


				}
				cargados = true;

			}catch(Exception e){
				e.printStackTrace();
			}

		}

		return cargados;
	}

}

