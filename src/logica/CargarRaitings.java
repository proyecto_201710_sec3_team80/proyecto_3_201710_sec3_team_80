package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import model.data.structures.ArbolBinario;
import model.data.structures.ListaEncadenada;
import modelVO.VORating;

public class CargarRaitings {

	public static boolean cargarRaiting(String ruta, ArbolBinario<Long, ArbolBinario<Long, VORating>> arbolRaitings){
		boolean cargado = false;

		try{
			BufferedReader br = new BufferedReader(new FileReader(ruta));

			String line = br.readLine();
			VORating temp;
			String[] dataSplit;
			line = br.readLine();
			Long idusuario;
			ArbolBinario<Long, VORating> arbolAux;
			
			while(line != null){
				temp = new VORating();
				dataSplit = line.split(",");
				idusuario = Long.parseLong(dataSplit[0]);
				temp.setIdUsuario(idusuario);
				temp.setIdPelicula(Long.parseLong(dataSplit[1]));
				temp.setRating(Long.parseLong(dataSplit[2]));
				
				arbolAux = arbolRaitings.get(idusuario);
				
				if(arbolAux == null){
					arbolAux = new ArbolBinario<>();
					arbolAux.put(temp.getIdPelicula(), temp);
					arbolRaitings.put(idusuario, arbolAux);
				}else{
					arbolAux.put(temp.getIdPelicula(), temp);
				}
				
				line  = br.readLine();
			}

			cargado = true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}


		return cargado;
	}
}
