package logica;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.sun.javafx.event.DirectEvent;

import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.EncadenamientoSeparadoTH;
import recorridos.DirectedDFS;

public class CargarRedTeatros {

	public static boolean cargarRedTeatro(EncadenamientoSeparadoTH<String, Integer> tabla, EdgeWeightedDigraph G,String ruta){
		
		boolean cargado = false;
		JsonParser parser = new JsonParser();
		
		try{
			JsonElement jelement = parser.parse(new FileReader(ruta));
			JsonArray jArray = jelement.getAsJsonArray();
			
			JsonObject JdatosArco;
			String nombreV;
			String nombreW;
			double peso;
			
			JsonPrimitive jNombreV;
			JsonPrimitive jNombreW;
			JsonPrimitive jPeso;
			
			int v;
			int w;
			
			DirectedEdge arco;
			for(int i =0; i < jArray.size(); i++){
				JdatosArco = jArray.get(i).getAsJsonObject();
				
				jNombreV = JdatosArco.getAsJsonPrimitive("Teatro 1");
				jNombreW  = JdatosArco.getAsJsonPrimitive("Teatro 2");
				jPeso = JdatosArco.getAsJsonPrimitive("Tiempo (minutos)");
				
				nombreV = jNombreV.getAsString();
				nombreW = jNombreW.getAsString();
				peso = jPeso.getAsDouble();
				
				v = tabla.darValor(nombreV);
				w = tabla.darValor(nombreW);
				
				arco = new DirectedEdge(v, w, peso);
				
				G.addEdge(arco);
				
				
			}
			
			for(int j =0; j < tabla.darTamanio(); j++){
				arco = new DirectedEdge(j, j, 0.0);
				G.addEdge(arco);
			}
			
			cargado = true;
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return cargado;
	}
	

}