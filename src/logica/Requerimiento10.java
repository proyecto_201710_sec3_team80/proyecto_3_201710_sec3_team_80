package logica;

import java.util.Iterator;

import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.ListaEncadenada;
import modelVO.VOTeatro;

public class Requerimiento10 {

	private boolean[] marked;
	private int[] id;
	private int count;
	
	public Requerimiento10(EdgeWeightedDigraph G, int max, ListaEncadenada<ListaEncadenada<Integer>> caminos, int v )
	{
		ListaEncadenada<Integer> camino;
		count = 0;
		marked = new boolean[G.V()];
		id = new int[G.V()];
		
		Iterator<DirectedEdge> iterAux;
		Iterator<DirectedEdge> iter;
		for(DirectedEdge e: G.adj(v))
		{
			iter = G.adj(v).iterator();
			
			for(int i =0; i < count; i++){
				iter.next();
			}
			iterAux = iter;
			camino = new ListaEncadenada<>();
			dfs(G, v, camino, max, iterAux);
			count++;
			caminos.agregarElementoFinal(camino);
		}
		
	}

	
	private void dfs(EdgeWeightedDigraph G, int v, ListaEncadenada<Integer> camino, int max, Iterator<DirectedEdge> iter)
	{
		int id1 = 0;
		int id2 = 0;
		marked[v] = true;
		id[v] = count;
		camino.agregarElementoFinal(v);
		while(iter.hasNext()){
		DirectedEdge e = iter.next();
			if(camino.darNumeroElementos() < max)
			{
				id1 = id[v];
				id2 = id[e.to()];
					dfs(G, e.to(), camino, max,iter);
			}
			else
				break;
		}
	}
	
	public boolean connected(int v, int w)
	{ return id[v] == id[w]; }
	
	public int id(int v)
	{ return id[v]; }
	
	public int count()
	{ return count; }
	
}
