package logica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.ArbolBinario;
import model.data.structures.ListaEncadenada;
import modelVO.VOPelicula;

public class CargarPeliculas {

	public static boolean cargarPelicula(String ruta, ArbolBinario<Long,VOPelicula> peliculas){
		boolean cargado = false;
		JsonParser parser = new JsonParser();
		try{
			
			JsonElement jelement = parser.parse(new FileReader(ruta));
			JsonArray jArray = jelement.getAsJsonArray();
			
			Long movie_id;
			String title;
			String[] generos;			
			
			JsonPrimitive jMovie_id;
			JsonPrimitive jTitle;
			JsonPrimitive jGeneros;
			
			JsonObject jobject;
			VOPelicula pelicula;
			
			for(int i =0; i < jArray.size(); i++){
				jobject = jArray.get(i).getAsJsonObject();
				
				jMovie_id = jobject.getAsJsonPrimitive("movie_id");
				movie_id = jMovie_id.getAsLong();
				
				jTitle = jobject.getAsJsonPrimitive("title");
				title = jTitle.getAsString();
				
				jGeneros = jobject.getAsJsonPrimitive("genres");
				generos = jGeneros.getAsString().split("[|]");
				
				pelicula = new VOPelicula(movie_id, title, generos);
			  peliculas.put(movie_id,pelicula);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return cargado;
	}
	
}
