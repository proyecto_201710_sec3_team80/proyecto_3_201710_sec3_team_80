package logica;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
public class GenerarSubGrafoFranq 
{
	public static void generarSubGrafo( EdgeWeightedDigraph G, String franquicia, FestivalCine festival, int s )
	{
		JsonParser parser = new JsonParser();
		
		try
		{
			JsonElement jelement = parser.parse(new FileReader("./data/tiemposIguales_v2.json"));
			JsonArray jArray = jelement.getAsJsonArray();
			
			JsonObject JdatosArco;
			String nombreV;
			String nombreW;
			double peso;
			
			JsonPrimitive jNombreV;
			JsonPrimitive jNombreW;
			JsonPrimitive jPeso;
			
			int v;
			int w;
			
			DirectedEdge arco;
			for(int i =0; i < jArray.size(); i++){
				JdatosArco = jArray.get(i).getAsJsonObject();
				
				jNombreV = JdatosArco.getAsJsonPrimitive("Teatro 1");
				jNombreW  = JdatosArco.getAsJsonPrimitive("Teatro 2");
				jPeso = JdatosArco.getAsJsonPrimitive("Tiempo (minutos)");
				
				nombreV = jNombreV.getAsString();
				nombreW = jNombreW.getAsString();
				peso = jPeso.getAsDouble();
				
				v = festival.index(nombreV);
				w = festival.index(nombreW);
				
				if( franquicia.equals(festival.darVOTeatro(v).getFranquicia().getNombre()) && franquicia.equals(festival.darVOTeatro(w).getFranquicia().getNombre()) )
				{
					arco = new DirectedEdge(v, w, peso);
					
					G.addEdge(arco);
					
					s = v;
				}
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
