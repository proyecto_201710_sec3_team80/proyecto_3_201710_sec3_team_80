package logica;

import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.EncadenamientoSeparadoTH;

public class CargarSimilitudes 
{
	
	public static boolean cargarSimilitudes(String ruta, EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> similitudes)
	{
		boolean cargado = false;

		JsonParser parser = new JsonParser(); 
		try
		{
			cargarTabla(similitudes);

			JsonElement jelement = parser.parse(new FileReader(ruta));
			JsonArray jArray = jelement.getAsJsonArray();
			
			JsonObject jobject;
			
			JsonPrimitive sim;
			String simTemp;
			
			Long idActual;
			JsonPrimitive id;
			
			
			for(int i =0; i < jArray.size(); i++){

				jobject = jArray.get(i).getAsJsonObject();
				
				id = jobject.getAsJsonPrimitive("movieId");
				idActual = id.getAsLong();
				
				sim = jobject.getAsJsonPrimitive("493405");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long)493405, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 493405, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("498381");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 498381, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 498381, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("1219827");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 1219827, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 1219827, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("1293847");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 1293847, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 1293847, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("1691916");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 1691916, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 1691916, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("1730768");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 1730768, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 1730768, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("1753383");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 1753383, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 1753383, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2072233");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2072233, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2072233, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2398241");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2398241, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2398241, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2568862");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2568862, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2568862, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2582576");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2582576, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2582576, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2763304");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2763304, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2763304, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("2771200");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 2771200, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 2771200, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3171832");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3171832, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3171832, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3315342");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3315342, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3315342, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3401882");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3401882, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3401882, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3405236");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3405236, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3405236, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3462710");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3462710, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3462710, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3717490");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3717490, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3717490, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3731562");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3731562, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3731562, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3874544");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3874544, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3874544, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3896198");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3896198, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3896198, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("3922818");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 3922818, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 3922818, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4030600");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4030600, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4030600, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4116284");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4116284, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4116284, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4217392");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4217392, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4217392, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4287320");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4287320, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4287320, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4425200");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4425200, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4425200, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4465564");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4465564, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4465564, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4481414");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4481414, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4481414, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4581576");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4581576, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4581576, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4630562");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4630562, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4630562, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("4849438");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 4849438, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 4849438, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5052448");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5052448, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5052448, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5155780");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5155780, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5155780, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5442430");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5442430, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5442430, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5460276");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5460276, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5460276, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5607714");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5607714, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5607714, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5710514");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5710514, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5710514, Double.parseDouble(simTemp));
				}
				
				sim = jobject.getAsJsonPrimitive("5982852");
				simTemp = sim.getAsString();
				
				if( simTemp.equals("NaN"))
				{
					similitudes.darValor(idActual).insertar((long) 5982852, null);
				}
				else
				{
					similitudes.darValor(idActual).insertar((long) 5982852, Double.parseDouble(simTemp));
				}
				
			}
			
			cargado = true;

			System.out.println("Se cargaron las salas a partir del archivo, simMatriz.json");
		}
		catch(Exception e){
			e.printStackTrace();
		}


		return cargado;
	}

	private static void cargarTabla( EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> similitudes )
	{
		
		EncadenamientoSeparadoTH<Long, Double> aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 493405, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 498381, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 1219827, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 1293847, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 1691916, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 1730768, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 1753383, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2072233, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2398241, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2568862, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2582576, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2763304, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 2771200, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3171832, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3315342, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3401882, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3405236, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3462710, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3717490, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3731562, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3874544, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3896198, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 3922818, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4030600, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4116284, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4217392, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4287320, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4425200, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4465564, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4481414, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4581576, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4630562, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 4849438, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5052448, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5155780, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5442430, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5460276, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5607714, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5710514, aux );
		
		aux = new EncadenamientoSeparadoTH<>(40);
		similitudes.insertar((long) 5982852, aux );
		
	}

}
