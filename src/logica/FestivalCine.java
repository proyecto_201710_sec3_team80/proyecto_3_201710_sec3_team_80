package logica;

import java.io.BufferedReader;
import java.util.Iterator;

import com.sun.scenario.effect.impl.HeapImage;

import API.IEdge;
import API.ILista;
import API.ISistemaRecomendacion;
import model.data.structures.ArbolBinario;
import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.EncadenamientoSeparadoTH;
import model.data.structures.ListaEncadenada;
import model.data.structures.MaxPQ;
import model.data.structures.MinHeap;
import modelVO.VOFranquicia;
import modelVO.VOGeneroPelicula;
import modelVO.VOPelicula;
import modelVO.VOPeliculaPlan;
import modelVO.VORating;
import modelVO.VOTeatro;
import modelVO.VOUsuario;
import recorridos.DijkstraSP;
import recorridos.DirectedDFS;
import recorridos.MaxMatchingEdmonds;

public class FestivalCine implements ISistemaRecomendacion{

	private EncadenamientoSeparadoTH<String, Integer> posicionesGrafo;
	private ListaEncadenada<VOTeatro> listaNombresTeatros;
	private VOTeatro[] infoVerticesTeatro; 
	private EdgeWeightedDigraph redTeatros;
	private DijkstraSP dijkstra;
	private DirectedDFS dfs;
	private ArbolBinario<Long, ArbolBinario<Long, VORating>> arbolRaitings;
	private ArbolBinario<Long, VOPelicula> arbolPeliculas;
	private EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> similitudes;

	public FestivalCine()
	{
		posicionesGrafo = new EncadenamientoSeparadoTH<String, Integer>(5);
		listaNombresTeatros = new ListaEncadenada<VOTeatro>();

	}



	public boolean contains(String s){return posicionesGrafo.tieneLlave(s);}

	public int index(String s){return posicionesGrafo.darValor(s);}
	public VOTeatro darVOTeatro(int v){return infoVerticesTeatro[v];}
	//public EdgeWeightedDigraph darGrafo(){return redTeatros;}
	public EdgeWeightedDigraph darGrafoTeatros(){return redTeatros;}
	public ArbolBinario<Long, ArbolBinario<Long, VORating>> darArbolRaitings(){return arbolRaitings;}
	public ArbolBinario<Long, VOPelicula> darArbolPeliculas(){return arbolPeliculas;}
	public EncadenamientoSeparadoTH<Long, EncadenamientoSeparadoTH<Long, Double>> darSimilitudes(){return similitudes;}
	public EncadenamientoSeparadoTH<String, Integer> darPosicionesGrafo( ) {return posicionesGrafo; }


	@Override
	public boolean cargarTeatros(String ruta) {
		// TODO Auto-generated method stub
		boolean cargado = false;
		cargado = CargarTeatro.cargarTeatros(ruta,listaNombresTeatros,posicionesGrafo);

		if(cargado == true){
			infoVerticesTeatro = new VOTeatro[listaNombresTeatros.darNumeroElementos()];

			for(VOTeatro teatro: listaNombresTeatros){
				infoVerticesTeatro[index(teatro.getNombre())] = teatro;
			}
		}
		return cargado;
	}


	public boolean cargarSimilitudes( String rutaSimilitudes )
	{
		similitudes = new EncadenamientoSeparadoTH<>(40);
		return CargarSimilitudes.cargarSimilitudes(rutaSimilitudes, similitudes);
	}

	public boolean cargarRaitings(String rutaRaitings){

		arbolRaitings = new ArbolBinario<>();
		return CargarRaitings.cargarRaiting(rutaRaitings, arbolRaitings);
	}

	public boolean cargaPeliculas(String rutaPeliculas){

		arbolPeliculas = new ArbolBinario<>();
		boolean cargadas = CargarPeliculas.cargarPelicula(rutaPeliculas, arbolPeliculas);
		return cargadas;
	}



	public boolean cargarCartelera(ListaEncadenada<String> rutas) {
		// TODO Auto-generated method stub

		boolean cargadas = CargarCarteleras.cargarInfoCarteleras(rutas, infoVerticesTeatro, posicionesGrafo, arbolPeliculas);
		return cargadas;
	}



	@Override
	public boolean cargarRed(String ruta) {
		redTeatros = new EdgeWeightedDigraph(posicionesGrafo.darTamanio());
		return CargarRedTeatros.cargarRedTeatro(posicionesGrafo, redTeatros, ruta);
	}



	@Override
	public int sizeMovies() {
		return arbolPeliculas.size();
	}



	@Override
	public int sizeTeatros() {
		return infoVerticesTeatro.length;
	}



	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		// TODO Auto-generated method stub

		MaxPQ<VORating> recomendaciones = new MaxPQ<VORating>();
		GenerarRecomendaciones.generarRecomendaciones(usuario, recomendaciones, this);
		Requerimiento4 req4 = new Requerimiento4();
		return req4.run(usuario, fecha, this,recomendaciones);
	}



	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		MaxPQ<VORating> recomendaciones = new MaxPQ<VORating>();
		GenerarRecomendaciones.generarRecomendaciones(usuario, recomendaciones, this);
		int n = recomendaciones.size();
		VORating aux;
		ListaEncadenada<VORating> listaPeliculasGenero = new ListaEncadenada<>();
		String[] generos;
		VOPelicula peliAux;
		for(int i=0; i < n; i++){
			aux = recomendaciones.delMax();
			peliAux = arbolPeliculas.get(aux.getIdPelicula());
			generos = peliAux.getGeneros();
			for(int j =0; j < generos.length; j++){
				if(generos[j].equals(genero.getNombre())){
					listaPeliculasGenero.agregarElementoFinal(aux);
				}
			}
		}
		Requerimiento5 req5 = new Requerimiento5();
		return req5.run(this,listaPeliculasGenero);
	}



	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(String franquicia, int fecha, String franja, VOUsuario usuario) {

		MaxPQ<VORating> maxheapRec = new MaxPQ<>(10);

		GenerarRecomendaciones.generarRecomendaciones(usuario, maxheapRec, this);

		EdgeWeightedDigraph subgrafo = new EdgeWeightedDigraph(redTeatros.V());

		int s = 0;
		GenerarSubGrafoFranq.generarSubGrafo( subgrafo, franquicia, this, s );

		Requerimiento6 req6 = new Requerimiento6();

		return req6.run( fecha, this, maxheapRec, franja, subgrafo);
	}



	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha ) {


		VORating aux;
		ListaEncadenada<VOPelicula> listaPeliculas = new ListaEncadenada<VOPelicula>();
		arbolPeliculas.inOrderLista(listaPeliculas);
		ListaEncadenada<VOPelicula> listaPeliculasMismoGenero = new ListaEncadenada<>();
		String[] generos;
		for(VOPelicula pelicula: listaPeliculas){
			generos = pelicula.getGeneros();
			for(int j =0; j < generos.length; j++){
				if(generos[j].equals(genero.getNombre())){
					listaPeliculasMismoGenero.agregarElementoFinal(pelicula);
				}
			}
		}
		Requerimiento7 req7 = new Requerimiento7();
		return req7.run(this,listaPeliculasMismoGenero,fecha);
	}



	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			String franquicia) {

		EdgeWeightedDigraph subgrafo = new EdgeWeightedDigraph(redTeatros.V());

		int s = 0;
		GenerarSubGrafoFranq.generarSubGrafo( subgrafo, franquicia, this, s );

		ListaEncadenada<VORating> listaPeliculasGeneri = new ListaEncadenada<>();

		ListaEncadenada<VOPelicula> inOrden = new ListaEncadenada<>();
		arbolPeliculas.inOrderLista(inOrden);

		VORating peli = new VORating();

		for( VOPelicula p: inOrden )
		{
			for( String g: p.getGeneros() )
			{
				if(g.equals(genero.getNombre()))
				{
					peli = new VORating();
					peli.setIdPelicula(p.getId());
					listaPeliculasGeneri.agregarElementoFinal(peli);
				}
			}
		}

		Requerimiento8 req8 = new Requerimiento8();

		return req8.run(this, listaPeliculasGeneri, subgrafo);

	}



	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		// TODO Auto-generated method stub
		ListaEncadenada<DirectedEdge> arcos = new ListaEncadenada<>();
		MaxMatchingEdmonds.maxMatching(redTeatros,arcos);
		for(DirectedEdge e: arcos){
			System.out.println("From: " +  darVOTeatro(e.from()).getNombre() + " to: "+  darVOTeatro(e.to()).getNombre());
		} 
		return  null;
	}



	@Override
	public ListaEncadenada<ListaEncadenada<VOTeatro>> rutasPosible(String origen, int n) {

		ListaEncadenada<ListaEncadenada<Integer>> caminos = new ListaEncadenada<>();
		int v = posicionesGrafo.darValor(origen);
		Requerimiento10 req10 = new Requerimiento10(redTeatros, n, caminos, v);

		ListaEncadenada<ListaEncadenada<VOTeatro>> caminosTeatro = new ListaEncadenada<>();
		ListaEncadenada<VOTeatro> camino;
		for( ListaEncadenada<Integer> lista: caminos )
		{
			camino = new ListaEncadenada<>();
			for( Integer w: lista )
			{
				camino.agregarElementoFinal(infoVerticesTeatro[w]);
			}
			caminosTeatro.agregarElementoFinal(camino);
		}

		return caminosTeatro;
	}
}
