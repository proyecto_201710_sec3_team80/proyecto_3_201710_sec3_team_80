package logica;

import model.data.structures.ArbolBinario;
import model.data.structures.ListaEncadenada;
import model.data.structures.MaxPQ;
import modelVO.VORating;
import modelVO.VOUsuario;

public class GenerarRecomendaciones {

	public static boolean generarRecomendaciones(VOUsuario usuario, MaxPQ<VORating> recomendadas, FestivalCine festival)
	{

		boolean cargado = false;

		ArbolBinario<Long, VORating> ratingsUsuario = festival.darArbolRaitings().get(usuario.getID());
		double numerador = 0;
		double denominador = 0;
		double recomendacion = 0;

		VORating ratingRec;

		for( Long id: festival.darSimilitudes().keys() )
		{
			if(!ratingsUsuario.contains(id))
			{
				for( Long id2: festival.darSimilitudes().darValor(id).keys() )
				{
					if( ratingsUsuario.contains(id2) )
					{
						if(festival.darSimilitudes().darValor(id).darValor(id2) != null)
						{
							numerador += ((ratingsUsuario.get(id2).getRating())*(festival.darSimilitudes().darValor(id).darValor(id2)));
							denominador += festival.darSimilitudes().darValor(id).darValor(id2);	
						}

					}
				}

				if( denominador != 0 )
				{
					recomendacion = numerador/denominador;
				}

				ratingRec = new VORating();
				ratingRec.setIdPelicula(id);
				ratingRec.setPrediccion(recomendacion);
				ratingRec.setIdUsuario(usuario.getID());

				recomendadas.insert(ratingRec);
			}

		}

		return cargado;
	}
}
