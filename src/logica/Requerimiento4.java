package logica;

import javax.swing.event.CellEditorListener;

import model.data.structures.ArbolBinario;
import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.IndexMinPQ;
import model.data.structures.ListaEncadenada;
import model.data.structures.MaxPQ;
import model.data.structures.MinHeap;
import model.data.structures.MinPQDirectedEdge;
import model.data.structures.Stack;
import modelVO.VOPeliculaPlan;
import modelVO.VOPeliculasDia;
import modelVO.VORating;
import modelVO.VOTeatro;
import modelVO.VOUsuario;
import recorridos.DijkstraSP;

public class Requerimiento4 {

	private double horaActual = 8;
	private  VOTeatro teatroAux;
	private  VOPeliculasDia carteleraDia;
	private  ArbolBinario<Long,ListaEncadenada<VOPeliculaPlan>> peliculasFranja;
	private ArbolBinario<Long,VOPeliculaPlan> plan;
	private FestivalCine festival;
	private int dia;
	private ListaEncadenada<VORating> mayoresRecomendados;
	private long idUsuario;
	private long idPelicula;
	private ListaEncadenada<VOPeliculaPlan> planPeliculas;

	public ListaEncadenada<VOPeliculaPlan> run(VOUsuario usuario, int dia, FestivalCine festival, MaxPQ<VORating> recomendaciones){

		this.festival = festival;
		this.dia = dia;
		this.idUsuario = usuario.getID();

		mayoresRecomendados = new ListaEncadenada<>();
		planPeliculas = new ListaEncadenada<>();
		int n = recomendaciones.size();

		for(int i=0; i < n; i++){
			mayoresRecomendados.agregarElementoFinal(recomendaciones.delMax());
		}


		EdgeWeightedDigraph G = festival.darGrafoTeatros();


		//for(int i =0; i < festival.sizeTeatros(); i++ ){
		plan = new ArbolBinario<>();

		DijkstraSP dijkstraSP = new DijkstraSP(G, 0);
		DirectedEdge[] edgeTo = dijkstraSP.edgeTo();

		MinPQDirectedEdge<DirectedEdge> minHeapPeso = new MinPQDirectedEdge<>(edgeTo.length);

		for(int i =0; i < edgeTo.length; i++){
			if(edgeTo[i] != null)
				minHeapPeso.insert(edgeTo[i]);
		}


		DirectedEdge e;
		int horaSiguiente = 8;

		for(int i =0; i< n; i++){

			e = minHeapPeso.delMin();
			teatroAux = festival.darVOTeatro(e.to());
			carteleraDia = teatroAux.getDias().get(dia);
			if(horaSiguiente <12){
				peliculasFranja = carteleraDia.getMagnana();

				for(VORating recomendado: mayoresRecomendados){

					idPelicula = recomendado.getIdPelicula();

					if(peliculasFranja.contains(idPelicula)){
						for(VOPeliculaPlan funcion: peliculasFranja.get(idPelicula)){
							if(funcion.getHoraInicio() == horaSiguiente)
							{


								if(!plan.contains(idPelicula)){
									plan.put(idPelicula, funcion);
									horaActual += 2;
									horaActual += e.weight()/60;
									horaActual = Math.ceil(horaActual);
									if(funcion.getHoraFin() == 12){horaSiguiente+=2;}else{horaSiguiente = funcion.getHoraFin();}
									if(horaActual %2 != 0)horaActual++;
								}
							}

						}

					}
				}
			}
			if(12 <= horaSiguiente && horaSiguiente <18){
				peliculasFranja = carteleraDia.getTarde();

				for(VORating recomendado: mayoresRecomendados){

					idPelicula = recomendado.getIdPelicula();

					if(peliculasFranja.contains(idPelicula)){
						for(VOPeliculaPlan funcion: peliculasFranja.get(idPelicula)){
							if(funcion.getHoraInicio() == horaSiguiente)
							{

								if(!plan.contains(idPelicula)){
									plan.put(idPelicula, funcion);
									horaActual += 2;
									horaActual += (e.weight()+(e.weight()*0.15))/60;
									horaActual = Math.ceil(horaActual);
									if(funcion.getHoraFin() == 18){horaSiguiente+=2;}else{horaSiguiente = funcion.getHoraFin();}
									if(horaActual %2 != 0)horaActual++;
								}
							}
						}

					}
				}
			}
			if(18<= horaSiguiente && horaSiguiente <= 24){
				peliculasFranja = carteleraDia.getNoche();

				for(VORating recomendado: mayoresRecomendados){

					idPelicula = recomendado.getIdPelicula();

					if(peliculasFranja.contains(idPelicula)){
						for(VOPeliculaPlan funcion: peliculasFranja.get(idPelicula)){
							if(funcion.getHoraInicio() == horaSiguiente)
							{

								if(!plan.contains(idPelicula)){
									horaSiguiente+=2;
									plan.put(idPelicula, funcion);
									horaActual += 2;
									horaActual += (e.weight()+(e.weight()*0.20))/60;
									horaActual = Math.ceil(horaActual);
									if(horaActual %2 != 0)horaActual++;
								}
							}
						}

					}
				}
			}
			else{
				break;
			}
			horaSiguiente+=2;

		}

		//}
		plan.inOrderLista(planPeliculas);

		return planPeliculas;
	}

}
