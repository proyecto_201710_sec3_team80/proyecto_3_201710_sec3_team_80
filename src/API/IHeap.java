//Copyright � 2000�2016, Robert Sedgewick and Kevin Wayne. 
//Last updated: Sat Jan 28 06:55:20 EST 2017.

package API;


public interface IHeap <T extends Comparable<T>>{
	
	public boolean isEmpty();
	
	public int size();
	
	public T delMin();

	public void insert(T item);
	
	
	
}
