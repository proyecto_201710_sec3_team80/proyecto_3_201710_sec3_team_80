package API;

public interface IDirectedEdge {

	/**
	 * Peso de este arco
	 * @return peso del arco
	 */
	public double weight();
	
	/**
	 * vertice desde el cual sale este arco
	 * @return
	 */
	public int from();

	/**
	 * Vertice al cual llega este vertice
	 * @return
	 */
	public int to();
	
}
