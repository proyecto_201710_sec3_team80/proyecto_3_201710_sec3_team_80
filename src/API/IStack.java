package API;

public interface IStack<T> {

	
	/**
	 * Agrega un item al tope de la fila
	 * @param item elemento que ser� agregado al tope de la fila
	 */
	public void push(T item);
	
	/**
	 * Elimina el elemento en el tope de la pila
	 * @return elemento que esta al tope de la pila y ser� eliminado de la misma
	 */
	public T pop();
	
	/**
	 * Indica si la pila est� vac�a
	 * @return true si la pila est� vac�a, false de lo contrar�o
	 */
	public boolean isEmpty();
	/**
	 * N�mero de elementos de la pila
	 * @return retorna la cantidad de elementos que contiene la pila
	 */
	public int size();
}
