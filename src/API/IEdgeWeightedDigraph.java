package API;

import model.data.structures.DirectedEdge;

public interface IEdgeWeightedDigraph {

	/**
	 * Numero de vertices
	 * @return numero de vertices
	 */
	public int V();
	
	/**
	 * Numero de arcos
	 * @return numero de arcos
	 */
	public int E();
	
	/**
	 * Agrega un arco e a este grafo
	 * @param e
	 */
	public void addEdge(DirectedEdge e);
	
	/**
	 * arcos incidentes a v
	 * @param v
	 * @return
	 */
	public Iterable<DirectedEdge> adj(int v);
	
	/**
	 * Todos los arcos de este grafo
	 * @return
	 */
	public Iterable<DirectedEdge> edges();
}
