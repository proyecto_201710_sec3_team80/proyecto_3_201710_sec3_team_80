package Controller;

import API.IEdge;
import API.ILista;
import logica.FestivalCine;
import model.data.structures.ListaEncadenada;
import modelVO.VOFranquicia;
import modelVO.VOGeneroPelicula;
import modelVO.VOPeliculaPlan;
import modelVO.VOTeatro;
import modelVO.VOUsuario;

public class Controlador{
	
	private static FestivalCine festival = new FestivalCine();

	public static boolean cargarDatos(String rutaRaiting, String rutaPeliculas, String rutaSimilitudes){
		
		boolean raiting = festival.cargarRaitings(rutaRaiting);
		boolean pelicula = festival.cargaPeliculas(rutaPeliculas);
		boolean similitudes = festival.cargarSimilitudes(rutaSimilitudes);
		return raiting && pelicula && similitudes;
		
	}
	
	public static boolean cargarTeatros(String ruta) {
		// TODO Auto-generated method stub
		return festival.cargarTeatros(ruta);
	}

	public static boolean cargarCartelera(ListaEncadenada<String> rutas) {
		// TODO Auto-generated method stub
		return festival.cargarCartelera(rutas);
	}

	public static boolean cargarRed(String ruta) {
		// TODO Auto-generated method stub
		return festival.cargarRed(ruta);
	}

	public  static int sizeMovies() {
		// TODO Auto-generated method stub
		return festival.sizeMovies();
	}

	public static int sizeTeatros() {
		// TODO Auto-generated method stub
		return festival.sizeTeatros();
	}

	public static ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		// TODO Auto-generated method stub
		return festival.PlanPeliculas(usuario, fecha);
	}

	public static ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		return festival.PlanPorGenero(genero, usuario);
	}

	public static ILista<VOPeliculaPlan> PlanPorFranquicia(String franquicia, int fecha, String franja, VOUsuario usuario) {
		// TODO Auto-generated method stub
		return festival.PlanPorFranquicia(franquicia, fecha, franja, usuario);
	}

	public static ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return festival.PlanPorGeneroYDesplazamiento(genero, fecha);
	}

	public static ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			String franquicia) {
		// TODO Auto-generated method stub
		return festival.PlanPorGeneroDesplazamientoYFranquicia(genero, fecha, franquicia);
	}

	public static ILista<IEdge<VOTeatro>> generarMapa() {
		// TODO Auto-generated method stub
		return festival.generarMapa();
	}

	public static ListaEncadenada<ListaEncadenada<VOTeatro>> rutasPosible(String origen, int n) {
		// TODO Auto-generated method stub
		return festival.rutasPosible(origen, n);
	}

}
