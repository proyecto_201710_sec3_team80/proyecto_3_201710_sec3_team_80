package test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import recorridos.DijkstraSP;
import recorridos.DirectedDFS;
import model.data.structures.DirectedEdge;
import model.data.structures.EdgeWeightedDigraph;
import model.data.structures.ListaEncadenada;
import model.data.structures.Stack;

public class TestEdgeWeightedDigraph {

	EdgeWeightedDigraph grafo;

	public void setupEscenario1(){
		grafo = new EdgeWeightedDigraph(4);
	}

	public void setupEscenario2(){
		grafo = new EdgeWeightedDigraph(4);

		DirectedEdge arco1 = new DirectedEdge(0, 1, 0.2);
		DirectedEdge arco2 = new DirectedEdge(1, 3, 2.3);
		DirectedEdge arco3 = new DirectedEdge(0, 3, 2.1);
		DirectedEdge arco4 = new DirectedEdge(2, 1, 3.0);

		grafo.addEdge(arco1);
		grafo.addEdge(arco2);
		grafo.addEdge(arco3);
		grafo.addEdge(arco4);
	}
	@Test
	public void DijkstraTest(){
		setupEscenario2();
		
		Iterable<DirectedEdge> path;
		Iterator<DirectedEdge> iter;
		
		DijkstraSP dijkstra;
		
		 
		 
		 dijkstra = grafo.dijkstra(0);
	     path = dijkstra.pathTo(3);

		iter = path.iterator();
		int cont =0;
		DirectedEdge arco = null;
		
		while(iter.hasNext())
		{
			arco = iter.next();
			cont++;
		}
		
		assertEquals("Deber�a existir un solo arco para llegar a el vertice 3", 1, cont);
		double peso = arco.weight();
		double esperado = 2.1;
		assertEquals("El peso del arco deber�a ser 2.1", esperado , peso,0);


		 path = dijkstra.pathTo(2);
		 assertNull("No deber�a existir camino a el vertice 2 desde el vertice 0", path);
	}
	
	@Test
	public void DirectedDFS()
	{
		setupEscenario2();
		ListaEncadenada<Integer> lista = new ListaEncadenada<>();
		DirectedDFS dfs = new DirectedDFS(grafo, 0, lista);
		
		assertEquals("La lista deber�a tener tres elementos", 3, lista.darNumeroElementos());
		
	}
}