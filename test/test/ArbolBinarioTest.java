/**
 * 
 */
package test;

import junit.framework.TestCase;
import model.data.structures.ArbolBinario;

/**
 * @author juliana
 *
 */
public class ArbolBinarioTest extends TestCase
{
	private ArbolBinario<String, Integer> arbol;
	
	public void setupEscenario1( )
	{
		arbol = new ArbolBinario<>();
	}
	
	public void setupEscenario2( )
	{
		setupEscenario1();
		
		arbol.put("Gato", 1);
		arbol.put("Perro", 2);
		arbol.put("Casa", 3);
		arbol.put("Carro", 4);
		arbol.put("Oso", 5);
		
	}
	
	public void testInsertar( )
	{
		setupEscenario1();
		
		arbol.put("Gato", 1);
		arbol.put("Perro", 2);
		arbol.put("Casa", 3);
		arbol.put("Carro", 4);
		arbol.put("Oso", 5);
		
		assertTrue(arbol.contains("Gato"));
		assertTrue(arbol.contains("Perro"));
		assertTrue(arbol.contains("Casa"));
		assertTrue(arbol.contains("Carro"));
		assertTrue(arbol.contains("Oso"));
		assertFalse(arbol.isEmpty());
	}
	
	public void testEliminar( )
	{
		setupEscenario2();
		
		arbol.delete("Gato");
		arbol.delete("Perro");
		arbol.delete("Carro");
		arbol.delete("Casa");
		arbol.delete("Oso");
		
		assertFalse(arbol.contains("Gato"));
		assertFalse(arbol.contains("Perro"));
		assertFalse(arbol.contains("Casa"));
		assertFalse(arbol.contains("Carro"));
		assertFalse(arbol.contains("Oso"));
		assertTrue(arbol.isEmpty());
	}
	
	public void testEliminarMin( )
	{
		setupEscenario2();
		
		arbol.deleteMin( );
		
		assertFalse(arbol.contains("Carro"));
		
	}
	
	public void testEliminarMax( )
	{
		setupEscenario2();
		
		arbol.deleteMax( );
		
		assertFalse(arbol.contains("Perro"));
	}
	
	public void testGet( )
	{
		setupEscenario2();
		
		assertEquals( new Integer(4), arbol.get("Carro"));
		assertEquals( new Integer(2), arbol.get("Perro"));
		assertEquals( new Integer(5), arbol.get("Oso"));
		assertEquals( new Integer(1), arbol.get("Gato"));
		assertEquals( new Integer(3), arbol.get("Casa"));
	}

	
}
